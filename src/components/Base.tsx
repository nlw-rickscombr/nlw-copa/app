import { ReactNode } from 'react'
import { ImageBackground } from 'react-native'
import { VStack } from 'native-base'

import bg from '../assets/imgs/bg-screen.png'

interface BaseProps {
    children: ReactNode
}

const Base = ({ children }: BaseProps) => {
    return (
        <VStack flex={1} bgColor="gray.900">
            <ImageBackground source={bg} resizeMode="cover" style={{ flex: 1}}>
                {children}
            </ImageBackground>
        </VStack>
    )
}

export default Base
