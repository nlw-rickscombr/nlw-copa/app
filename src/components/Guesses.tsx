import { useState, useEffect } from 'react'
import { Box, FlatList, useToast } from 'native-base'

import Game, { GameProps} from './Game'
import EmptyMyPoolList from './EmptyMyPoolList'
import Loading from './Loading';
import { api } from '../services/api'

interface Props {
  poolId: string
  code: string
}

const Guesses = ({ poolId, code }: Props) => {
    const [isLoading, setIsLoading] = useState(true)
    const [gamesData, setGamesData] = useState<GameProps[]>([])
    const [firstTeamPoint, setFirstTeamPoint] = useState('')
    const [secondTeamPoint, setSecondTeamPoint] = useState('')

    const toast = useToast()

    async function getGames() {
        try {
            setIsLoading(true)

            const { data: { games }  } = await api.get(`/pools/${poolId}/games`)
            setGamesData(games)

        } catch (error) {
            console.log('Error getting Guesses ==>', error)
            toast.show({
                title: 'Não foi possível carregar os jogos',
                placement: 'top',
                bgColor: 'red.500'
            })
        } finally {
            setIsLoading(false)
        }
    }

    async function handleGuessConfirm(gameId: string) {
        try {
            if(!firstTeamPoint.trim() || !secondTeamPoint.trim()){
                return toast.show({
                    title: 'Informe o placar do jogo',
                    placement: 'top',
                    bgColor: 'red.500'
                })
            }

            await api.post(`/pools/${poolId}/games/${gameId}/guesses`, {
                firstTeamPoints: Number(firstTeamPoint),
                secondTeamPoints: Number(secondTeamPoint),
            })
            toast.show({
                title: 'Palpite salvo com sucesso!',
                placement: 'top',
                bgColor: 'green.500'
            })

            getGames()

        } catch (error) {
            console.log('Error confirm Guess ==>', error)
            toast.show({
                title: 'Não foi possível salvar o palpite',
                placement: 'top',
                bgColor: 'red.500'
            })
        }
    }

    useEffect(() => {
        getGames()
    },[])

    if(isLoading){
        return <Loading />
    }

    return (
        <FlatList
            data={gamesData}
            keyExtractor={item => item.id}
            renderItem={({ item }) => (
                <Game
                    data={item}
                    setFirstTeamPoints={setFirstTeamPoint}
                    setSecondTeamPoints={setSecondTeamPoint}
                    onGuessConfirm={() => handleGuessConfirm(item.id)}
                />
            )}
            _contentContainerStyle={{ pb: 10 }}
            ListEmptyComponent={() => <EmptyMyPoolList code={code} />}
        />
    )
}

export default Guesses
