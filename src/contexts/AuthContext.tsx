import { createContext, ReactNode, useState, useEffect } from 'react'
import * as AuthSession from 'expo-auth-session'
import * as WebBrowser from 'expo-web-browser'
import * as Google from 'expo-auth-session/providers/google'
import { api } from '../services/api'

WebBrowser.maybeCompleteAuthSession()

interface UserProps {
    name: string
    avatarUrl: string
}
interface AuthProviderProps {
    children: ReactNode
}

export interface AuthContextDataProps {
    user: UserProps
    isUserLoading: boolean
    signIn: () => Promise<void>
}

export const AuthContext = createContext({} as AuthContextDataProps)

export function AuthContextProvider({ children }: AuthProviderProps) {
    const [isUserLoading, setIsUserLoading] = useState(false)
    const [user, setUser] = useState<UserProps>({} as UserProps)

    const appAuthURL = AuthSession.makeRedirectUri({ useProxy: true })
    const [request, response, promptAsync] = Google.useAuthRequest({
        clientId: process.env.GOOGLE_CLIENT_ID,
        redirectUri: appAuthURL,
        scopes: ['profile', 'email']
    })


    async function signIn() {
        try {
            setIsUserLoading(true)
            await promptAsync()

        } catch (error) {
            console.log('Erro SignIn => ', error)
            throw error
        } finally {
            setIsUserLoading(false)
        }
    }

    async function signInWithGoogle(access_token:string) {
        try {
            setIsUserLoading(true)
            const {data: { token: userToken }} = await api.post('/auth/users', {
                access_token
            })
            api.defaults.headers.common['Authorization'] = `Bearer ${userToken}`

            const {data: { user: userInfo }} = await api.get('/auth/me')
            setUser(userInfo)
            console.log('USER ===>', user)

        } catch (error) {
            console.log('SignIn with Google [ERROR]==>', error)
            throw error
        } finally {
            setIsUserLoading(false)
        }
    }
    useEffect(() => {
        if(response?.type === 'success' && response.authentication?.accessToken){
            signInWithGoogle(response.authentication.accessToken)
        }
    }, [response])

    return (
        <AuthContext.Provider value={{
            signIn,
            isUserLoading,
            user
        }}>
            {children}
        </AuthContext.Provider>
    )
}
