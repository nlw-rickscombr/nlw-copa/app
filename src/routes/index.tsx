import { NavigationContainer } from '@react-navigation/native'
import { Box } from 'native-base'
import { userAuth } from '../hooks/userAuth'
import { AppRoutes } from './app.routes'
import Signin from '../screens/Sigin'

export function Routes() {
    const { user } = userAuth()

    return (
        <Box flex={1} bg="gray.900">
            <NavigationContainer>
                {user.name ? <AppRoutes /> : <Signin />}
            </NavigationContainer>
        </Box>
    )
}
