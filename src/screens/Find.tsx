import { useState } from 'react'
import { useNavigation } from '@react-navigation/native'
import { Heading, useToast, VStack } from 'native-base'

import Header from '../components/Header'
import Base from '../components/Base'
import Input from '../components/Input'
import Button from '../components/Button'
import { api } from '../services/api'

const Find = () => {
    const [loading, setLoading] = useState(false)
    const [code, setCode] = useState('')

    const toast = useToast()
    const { navigate } = useNavigation()

    async function handleJoinPool() {
        try {
            setLoading(true)

            if(!code.trim()){
                return toast.show({
                    title: 'Informe um código de bolão',
                    placement: 'top',
                    bgColor: 'red.500'
                })
            }

            await api.post('/pools/join', {
                code
            })

            toast.show({
                title: 'Você entrou no bolão com sucesso!',
                placement: 'top',
                bgColor: 'green.500'
            })
            navigate('pools')

        } catch (error) {
            setLoading(false)
            console.log('ERRO Getting Pools by Code ==>', error)
            if(error.response?.data?.status === 404){
                return toast.show({
                    title: 'Bolão não encontrado!',
                    placement: 'top',
                    bgColor: 'red.500'
                })
            }
            if(error.response?.data?.status === 400){
                return toast.show({
                    title: 'Você já está nesse bolão',
                    placement: 'top',
                    bgColor: 'orange.500'
                })
            }

            toast.show({
                title: 'Não foi possível encontrar o bolão',
                placement: 'top',
                bgColor: 'red.500'
            })
        }
    }
    return (
        <Base>
            <Header title='Buscar por código' showBackButton/>
            <VStack mt={8} mx={5} alignItems="center">
                <Heading fontFamily="heading" color="white" fontSize="xl" mb={8} textAlign="center">
                    Encontre um bolão através {'\n'} de seu código único
                </Heading>

                <Input
                    mb={2}
                    placeholder="Qual o código do bolão?"
                    onChangeText={setCode}
                    autoCapitalize="characters"
                />

                <Button title="Buscar Bolão" onPress={handleJoinPool} />
            </VStack>
        </Base>
    )
}

export default Find
