import { useState } from 'react'
import { Heading, Text, VStack, useToast } from 'native-base'
import Header from '../components/Header'
import Base from '../components/Base'
import Input from '../components/Input'
import Button from '../components/Button'

import { api } from '../services/api'

import Logo from '../assets/imgs/logo.svg'

const NewPool = () => {
    const [title, setTitle] = useState('')
    const [loading, setLoading] = useState(false)
    const toast = useToast()

    async function handlePoolCreate() {
        if(!title){
            return toast.show({
                title: 'Informe um nome para seu bolão',
                placement: 'top',
                bgColor: 'red.500'
            })
        }

        try {
            setLoading(true)
            api.post('/pools', {
                title: title.toUpperCase()
            })
            toast.show({
                title: 'Bolão criado com sucesso!',
                placement: 'top',
                bgColor: 'green.500'
            })
            setTitle(null)

        } catch (error) {
            console.log('ERROR ==>')
            toast.show({
                title: 'Não foi possível criar o bolão',
                placement: 'top',
                bgColor: 'red.500'
            })
        } finally {
            setLoading(false)
        }
    }

    return (
        <Base>
            <Header title='Criar novo bolão' />
            <VStack mt={8} mx={5} alignItems="center">
                <Logo />
                <Heading fontFamily="heading" color="white" fontSize="xl" my={8} textAlign="center">
                    Crie seu próprio bolão da copa {'\n'} e compartilhe entre amigos!
                </Heading>

                <Input
                    mb={2}
                    placeholder="Qual o nome do seu bolão?"
                    onChangeText={setTitle}
                    value={title}
                />

                <Button
                    title="Criar meu Bolão"
                    onPress={handlePoolCreate}
                    isLoading={loading}
                />
                <Text color="gray.200" fontSize="md" textAlign="center" px={10} mt={4}>
                    Após criar seu bolão, você receberá um código único que poderá
                    usar para convidar outras pessoas.
                </Text>
            </VStack>
        </Base>
    )
}

export default NewPool
