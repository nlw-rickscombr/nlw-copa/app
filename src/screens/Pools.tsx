import { useState, useCallback } from 'react'
import { useNavigation, useFocusEffect } from '@react-navigation/native'
import { FlatList, Icon, useToast, VStack } from 'native-base'

import Base from '../components/Base'
import Button from '../components/Button'
import Header from '../components/Header'
import Loading from '../components/Loading'
import PoolCard, { PoolCardPros } from '../components/PoolCard'
import EmptyPoolList from '../components/EmptyPoolList'
import { api } from '../services/api'

import { Octicons } from '@expo/vector-icons'

const Pools = () => {
    const [isLoading, setIsLoading] = useState(true)
    const [pools, setPools] = useState<PoolCardPros[]>([])
    const { navigate } = useNavigation()
    const toast = useToast()

    async function getPools() {
        try {
            setIsLoading(true)

            const {data: { data: { pools } }} = await api.get('/pools')
            setPools(pools)
        } catch (error) {
            console.log('ERROR GET POOLS ==>', error)
            toast.show({
                title: 'Não foi possível carregar os bolões',
                placement: 'top',
                bgColor: 'red.500'
            })
        } finally {
            setIsLoading(false)
        }
    }

    useFocusEffect(useCallback(() => {
        getPools()
    },[]))

    return (
        <Base>
            <Header title="Meus Bolões" />
            <VStack mt={6} mx={5} pb={4} mb={4} borderBottomWidth={1} borderBottomColor="gray.600">
                <Button
                    title="Buscar bolão por código"
                    leftIcon={<Icon as={Octicons} name="search" color="black" size="md" />}
                    onPress={ () => navigate('find') }
                />
            </VStack>
            {isLoading ? (
                <Loading />
            ) : (
                <FlatList
                    data={pools}
                    keyExtractor={item => item.id}
                    renderItem={({ item }) => (
                        <PoolCard
                            data={item}
                            onPress={
                                () => navigate('details', {
                                    id: item.id
                                })
                            }
                        />
                    )}
                    ListEmptyComponent={() => <EmptyPoolList />}
                    showsVerticalScrollIndicator={false}
                    _contentContainerStyle={{ pb: 10 }}
                    px={4}
                />
            )}


        </Base>
    )
}

export default Pools
